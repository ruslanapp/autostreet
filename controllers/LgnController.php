<?php
namespace app\controllers;

use app\models\EntryLogin;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
//use yii\helpers\BaseFileHelper;

class LgnController extends Controller{
    /*private $user = "user";
    private $userPass = "1234";
    private $admin = "admin";
    private $adminPass = "admin5555";*/


    public function behaviors()
    {
        return[
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' =>['@'],
                    ],
                ],
            ],
            'verbs' =>[
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' =>[
                'class' =>'yii\web\ErrorAction'
            ],
        ];
    }

    public function actionEnter()
    {

        if (!\Yii::$app->user->isGuest){
            return $this->goHome();
        }

        $data = new EntryLogin();   

        if($data->load(Yii::$app->request->post()) && $data->login())
        {
            return $this->goBack(['formationroute/choose']);
        }

        return $this->render('enter',[
            'data' => $data
        ]);

        /*  if($data->load(Yii::$app->request->post()) && $data->login())
        {
            $data = Yii::$app->request->post('EntryLogin');

            if($data['login'] == $this->user && $data['password'] == $this->userPass)
            {
                echo "user";
            }
            else if($data['login'] == $this->admin && $data['password'] == $this->adminPass)
            {
                echo "admin";
            }
            else
            {
                $this->goHome();
            }
        }
        else
        {
            return $this->render('enter',['data' => $data]);
        }*/
    }

    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }
}