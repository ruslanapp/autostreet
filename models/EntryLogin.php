<?php
namespace  app\models;

use Yii;
use yii\base\Model;

class EntryLogin extends Model{

    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    public function rules()
    {
        return[
            [['username', 'password'], 'required'],
            [['username', 'password'], 'trim'],
            [['username'],'string','length'=>[2,16]],
            [['password'],'string','max'=>20],
            ['rememberMe','boolean'],
            ['password','validatePassword']
        ];
    }


    public function  validatePassword($attribute, $params)
    {
        if(!$this->hasErrors()){
            
            $user = $this->getUser();

            if(!$user || !$user->validatePassword($this->password)){
                $this->addError($attribute, "Неверное имя или пароль.");
            }
        }
        
    }
    
    public function login()
    {
        if($this->validate()){
            return Yii::$app->user->login($this->getUser(),$this->rememberMe ? 3600*24*30:0);
        }
        return false;
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

}