<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?=Yii::getAlias('@web'); ?>/favicon.ico" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    $visibleUser = false;
    $visibleAdmin = false;
    //user -> id = 101, admin -> id = 100
    if(Yii::$app->user->id == '101')
    {
        $visibleUser = true;
    }
    elseif(Yii::$app->user->id == '100')
    {
        $visibleUser = true;
        $visibleAdmin = true;
    }

    NavBar::begin([
        'brandLabel' => 'Главная',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Формирование маршрута', 'url' => ['/formationroute/choose'], 'visible' => $visibleUser],
            ['label' => 'Добавление маршрутов', 'url' => ['/formationroute/add'],'visible' => $visibleAdmin],
            ['label' => 'Инструкция', 'url' => ['/formationroute/help'],'visible' => $visibleUser],

            Yii::$app->user->isGuest ? (
                ['label' => 'Войти', 'url' => ['/lgn/enter']]
            ) : (
                '<li>'
                . Html::beginForm(['/lgn/logout'], 'post')
                . Html::submitButton(
                    'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link pd-tp-mn']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Расчет маршрута <?= date('Y') ?></p>

      <!--  <p class="pull-right"><?/*= Yii::powered() */?></p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
