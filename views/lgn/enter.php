<?php
//Yii::$app->response->redirect('/yii2/web/');
use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\helpers\Url;
$this->title = "Авторизация";

use \app\assets\AppAdd;
AppAdd::register($this);
?>
<div class="site-login">
    <h2><?= Html::encode($this->title)?></h2>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
    ]); ?>

    <?= $form->field($data, 'username')->label('Имя пользователя')->textInput(['autofocus' => true]) ?>
    <?= $form->field($data, 'password')->passwordInput()->label('Пароль') ?>
    <?= $form->field($data, 'rememberMe')->checkbox(['label'=>'Запомнить меня']);?>
    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' =>'btn btn-primary','name' => 'login-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
